# ライターの皆様へ

日々の業務も忙しい中、ご協力ありがとうございます。  

ここに記載したとおり、  
ライターの皆様にはこちらのBitbucketサイトにて、  
執筆活動を行っていただきます。  
  
とはいえ、**いきなりやったことない環境で書けと言われても。。**  
という声もあるかと思いますので、  
オススメの執筆環境をお知らせさせていただきます。  

# ■使用リポジトリ

## Bitbucket


### [Wiki](https://www.google.co.jp/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwjx3q3mr_bRAhXEWrwKHWGdCAAQFggiMAE&url=https%3A%2F%2Fja.wikipedia.org%2Fwiki%2FBitbucket&usg=AFQjCNGp8ulhFDbVUb9ve825upOr6j2TZw&sig2=ktTpinDyPQZCIfhKU6TlOA&bvm=bv.146094739,d.dGc)より  

---
**Bitbucketはアトラシアンが運営している、  
ソフトウェア開発を主としたプロジェクトのための、  
ウェブベースのホスティングサービスである**  

---


なにはなくともこちらのアカウントを作らないと、執筆ができません。  
お手数ですが、なにかしらのメールアドレスでBitbucketのアカウントを作成をお願いします。  
アカウント作成はこちらから  
#### [Bitbucketアカウント作成](https://bitbucket.org/account/signup/)  

アカウントが作成できたらmomoseまでご連絡お願います。  

# ■使用言語

## Markdown(形式)

今回皆様には、[Markdown](http://www.markdown.jp/what-is-markdown/)という軽量なマークアップ言語を使用して執筆をお願いします。  
記法がHTMLよりも大分少ないので、覚えやすいです。  
昨今イケてるWeb系企業は**ドキュメントを書くならMarkdownだ**って感じの会社多いみたいです。  
これを機会に覚えて見て下さい。  
  
チートシートは[こちら](http://qiita.com/Qiita/items/c686397e4a0f4f11683d)  
少しずつ調べながら書いて下さい。  

# ■執筆ツール
## 1. PCの場合

## Visual Studio Code (エディタ)

現状、マイクロソフト製のオープンソースエディタ  
Visual Studio Code(VScode)が一番のオススメです。  

【参考】[Visual Studio Code を使って Markdown のプレビュー](http://qiita.com/poemn/items/8094c04bba86bd4fbe54)  
  
オススメポイントは、  
**Markdownでのプレビューが非常に簡単**にできます。  
ショートカットで` Ctrl + Shift + V `と押すと、一発でMarkdownプレビューが表示されます。  

↓↓ こんな感じ(左側がMarkdownテキスト、右側がプレビューね) ↓↓  
![](https://qiita-image-store.s3.amazonaws.com/0/44446/1d893290-c053-fc68-95c5-21a939f037da.png =x200)
またBitbucketなど、gitリポジトリへの連携もGUIで対応できます。  

### インストール方法

ダウンロード、インストールは下記から。  
#### [インストールリンク](https://code.visualstudio.com/)

VScodeの詳細な使い方  
momose作成  
#### [リンク](./vscode_manual/README_1.md)

## 2. スマートフォンの場合

**移動時間にできたらいいのに。**  

そんな方には、下記アプリを紹介します。  
Androidだけだけど。。  

## Mgit
スマートフォン内でgitが操作できます。  

リンク [Mgit](http://applion.jp/android/app/com.manichord.mgit/)

### 【使い方】

#### リポジトリのclone

1. `+`ボタンをクリック
1. 下記を入力して`クローンボタン`を押下  
  リモートURL : https://muonsay_develop@bitbucket.org/muonsay_develop/business_know-how.git  
  ローカルパス: 任意(基本デフォルト)  
  ユーザ名　　: Bitbucketで作成したアカウントID  
  パスワード　: 上記アカウントのパスワード  
1. ローカルにソースがダウンロードされる。  

#### 執筆

1. 修正したいファイルを開き、上部のペンマークを押下し、修正/保存  
  編集に別のアプリを使用する場合は、上部ドキュメントマーク(※)を押下  
1. 修正したファイルを長押しし、` Add to stage `を押下  
1. 右上` ︙`より` Commit `を選択  
1. コミットメッセージを追記し、` コミットボタン `を押下  
1. 右上` ︙`より` Push `を選択  
1. ` origin `を選択  
1. プッシュ結果が` Success push to remote ref. `と表示されたら、  
  Bitbucketに反映される。  

※ 編集には**JotterPad**が便利  
　Markdownのタグが聞いているかどうかがわかりやすく、プレビューも可能  
　注意: プレビュー機能は課金あり。。  
