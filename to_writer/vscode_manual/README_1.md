# VScode (インストール〜リポジトリダウンロード)

1. VScodeをインストールし、起動。左側のgitボタンを押下  
  ![start](../images/VScode_clone_commit_push/00.start_vscode.png)

1. gitがインストールされていない場合は、インストールを促される。  
  `git-scm.com`のリンクをクリックすると自動的にダウンロードが開始する。  
  gitのインストールは基本全部「次へ」でOK。  
  ※gitのインストールは説明省略します。  
  ![start](../images/VScode_clone_commit_push/01.git_menu_view.png)

1. gitインストール後、もう一度VScodeを起動します。  
  Welcomeメッセージが出ていたら☓で閉じて、
  `Ctrl + shift + P `を押します。
  ![start](../images/VScode_clone_commit_push/02.git_clone01.png)

1. `git:clone`を入力して、Enter  
  ![start](../images/VScode_clone_commit_push/03.git_clone02.png)

1. リポジトリのURLを入れ、Enterを押す。  
  ![start](../images/VScode_clone_commit_push/03.git_clone03.png)
  ![start](../images/VScode_clone_commit_push/03.git_clone04.png)

1. リポジトリデータをダウンロードするディレクトリを任意指定。  
  ![start](../images/VScode_clone_commit_push/04.select_dir.png)

1. bitbucketのユーザ名・パスワードを求められたら入力  
  ![start](../images/VScode_clone_commit_push/05_git_secury.png)

1. ダウンロード完了。  
  ![start](../images/VScode_clone_commit_push/06.git_clone_end.png)

# 編集〜COMMIT〜PUSHは[こちら](./README_2.md)
