# VScode (編集〜COMMIT〜PUSH)

続いて編集〜PUSHまでの流れです。  

1. VScodeを開いて、編集対象ファイルを開き編集する。  
  ![07-1.before](../images/VScode_clone_commit_push/07-1.before.png)

1. 編集/保存が完了したら、gitタブを開きます。  
  ![07-2.before](../images/VScode_clone_commit_push/07-2.after.png)

1. 編集したファイルが表示されるので、選択。  
  ![08.git_tab_select](../images/VScode_clone_commit_push/08.git_tab_select.png)

1. 修正内容が問題なければ、` + `ボタンを押し、ステージングに追加  
  ![09.check_diff.png](../images/VScode_clone_commit_push/09.check_diff.png)
  ![10.add_staging.png](../images/VScode_clone_commit_push/10.add_staging.png)

1. コメントを追加して、変更をコミット。  
  ![11.commit.png](../images/VScode_clone_commit_push/11.commit.png)

1. コミットした内容をpushでBitbucket側に反映  
  ※ PUSHが成功しても結果通知はなし..。エラーのときだけ通知あり。  
  ![12_push.png](../images/VScode_clone_commit_push/12_push.png)

1. サーバ側に変更内容が適用される。  
  ![13-01.push_success.png](../images/VScode_clone_commit_push/13-01.push_success.png)
  ![13-02.push_success.png](../images/VScode_clone_commit_push/13-02.push_success.png)

# インストール〜リポジトリダウンロードは[こちら](./README_1.md)
